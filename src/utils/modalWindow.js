/* eslint-disable default-case */
import React from 'react';

import SignIn from '../containers/SignIn/SignInContainer';
import SignUp from '../containers/SignUp/SignUpContainer';

const getComponent = (value) => {
	switch (value) {
		case 'sign_in':
			return <SignIn />
		case 'sign_up':
			return <SignUp />
		case 'forgot_password':
			return <></>
		case 'other_portails':
			return <></>
	}
}

export default getComponent;
