/* eslint-disable default-case */
export const getProps = (name, value, handleChange, component, placeholder) => {
	let type = '';

	switch (name) {
		case 'email':
			type = 'email';
			break;
		case 'password':
		case 'confirmPassword':
			type = 'password';
			break;
		case 'firstName':
		case 'lastName':
		case 'address':
			type = 'text';
			break;
	}

	return {
		id: name,
		name,
		type,
		value,
		handleChange: (e) => handleChange(e.target),
		component,
		placeholder: (placeholder || name),
	}
}