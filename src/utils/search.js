export const getInitialState = (VALUES) => {
	const state = {
		projects: {},
		grants: {},
		events: {},
	}

	Object.keys(VALUES.PROJECTS).forEach((item) => {
		state.projects[item] = VALUES.PROJECTS[item][0];
	})
	Object.keys(VALUES.GRANTS).forEach((item) => {
		state.grants[item] = VALUES.GRANTS[item][0];
	})
	Object.keys(VALUES.EVENTS).forEach((item) => {
		state.events[item] = VALUES.EVENTS[item][0];
	})

	return state;
}

export const getOptions = (array) => array.map(item => ({
	value: item,
	label: item,
}));