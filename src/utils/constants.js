export const CUSTOM_STYLES = {
	control: (provided, state) => ({
		position: 'relative',
		borderRadius: '25px',
		border: '1px solid',
		borderColor: state.menuIsOpen ? '#35d0de' : '#fff',
		fontSize: '18px',
		height: '50px',
		paddingLeft: '20px',
		fontWeight: 600,
		lineHeight: '50px',
		color: '#333',
		zIndex: state.menuIsOpen ? 50 : 40,
		cursor: 'default',
		backgroundColor: '#fff',
	}),
	valueContainer: (provided) => ({
		...provided,
		position: 'initial',
		padding: 0,
	})
}

export const ROUTES = {
	SIGN_UP: '/sign-up/',
}

export const VALUES = {
	SEARCH: {
		projects: {
			sort_by: [
				'Popularity',
				'Max supporters',
				'Min supporters',
				'Oldest',
				'Newest',
			],
			active_modules: [
				'Volunteering',
				'Petition',
				'Crowdfunding',
			],
		},
		grants: {

		},
		events: {

		},
	},
	SIGN_IN: {
		email: '',
		password: '',
	},
	SIGN_UP: {
		firstName: '',
		lastName: '',
		email: '',
		password: '',
		confirmPassword: '',
	},
}

export const BUTTONS = {
	AUTH_HEADER: [
		{
			name: "Create project",
			buttonStyle: "success",
			place: "header",
		},
		{
			name: "Create project",
			buttonStyle: "avatar",
			place: "header",
		},
	],
	NOT_AUTH_HEADER: [
		{
			name: "Start project",
			buttonStyle: "success",
			place: "header",
		},
		{
			name: "Sign In",
			buttonStyle: "primary",
			place: "header",
		},
		{
			name: "Join Tribus",
			buttonStyle: "primary",
			place: "header",
		},
	],
	SIGN_IN: [
		{
			name: "Forgot password?",
			buttonStyle: "link",
			place: "form_link",
		},
		{
			name: "Sign in",
			buttonStyle: "success",
			place: "form",
			type: "submit",
		},
		{
			name: "Create account",
			buttonStyle: "bordered",
			place: "form",
		}
	],
	SIGN_UP: [
		{
			name: "Create",
			buttonStyle: "success",
			place: "form",
			type: "submit",
		},
		{
			name: "Log in",
			buttonStyle: "bordered",
			place: "form",
		}
	],
}