import React, { Fragment, useState } from 'react';
import { Field } from 'redux-form'

import TextField from '../../Common/TextField/TextField';
import PasswordField from '../../Common/PasswordField/PasswordField';
import Button from '../../Common/Button/Button';

import { getProps } from '../../../utils/form';
import { VALUES, BUTTONS } from '../../../utils/constants';

import styles from './SignIn.module.scss';

const SignIn = ({ history, signIn, show }) => {
	const [state, setState] = useState({
		values: VALUES.SIGN_IN,
	});

	const handleChange = ({ name, value }) => {
		setState({
			...state,
			[name]: value,
		})
	}

	return (
		<Fragment>
			<h3 className={styles.title}>Sign in to share ideas and support others</h3>
			<form className={styles.form} onSubmit={() => signIn(history)}>
				{Object.keys(state.values).map((item, index) =>
					<Field
						key={index}
						header={true}
						{...getProps(item, state.values[item], handleChange, item.includes('password') ? PasswordField : TextField)}
					/>)}
				<Button {...BUTTONS.SIGN_IN[0]}/>
				<div className={styles.other_portails}>
					Business or local council? <span className={styles.portails_button}>Click here.</span>
				</div>
				<Button {...BUTTONS.SIGN_IN[1]}/>
				<Button {...BUTTONS.SIGN_IN[2]} handleClick={() => show('sign_up')}/>
			</form>
		</Fragment>
	);

};

export default SignIn;