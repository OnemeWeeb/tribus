/* eslint-disable default-case */
import React, { Fragment, useState } from 'react';
import { Link } from 'react-router-dom';
import { Field } from 'redux-form'

import TextField from '../../Common/TextField/TextField';
import Button from '../../Common/Button/Button';

import { getProps } from '../../../utils/form';
import { VALUES, BUTTONS } from '../../../utils/constants';

import styles from './SignUp.module.scss';
import PasswordField from '../../Common/PasswordField/PasswordField';

const SignUp = ({ history, signUp }) => {
	const [state, setState] = useState({
		values: VALUES.SIGN_UP,
		isShowing: false,
	});

	const handleChange = ({ name, value }) => {
		setState({
			...state,
			value: {
				...state.value,
				[name]: value,
			}
		})
	}

	return (
		<Fragment>
			<h3 className={styles.title}>Join the Tribus community</h3>
			<form className={styles.form} onSubmit={() => signUp(history)}>
				{Object.keys(state.values).map((item, index) => {
					if (index < 3) {
						return (
							<Field
								key={item}
								header={index === 2}
								{...getProps(item, state.values[item], handleChange, TextField)}
							/>
						)
					} else if (index % 2) {
						return (
							<div key={item} className={styles.row}>
								<div className={styles.column}>
									<Field
										header={true}
										isShowing={state.isShowing}
										handleClick={() => setState({ ...state, isShowing: !state.isShowing })}
										{...getProps(item, state.values[item], handleChange, PasswordField)}
									/>
								</div>
								<div className={styles.column}>
									<Field
										isShowing={state.isShowing}
										{...getProps(Object.keys(state.values)[index + 1], Object.values(state.values)[index + 1], handleChange, PasswordField)}
									/>
								</div>
							</div>
						)
					}
				})}
				<div className={styles.text}>
					{'Any personal information you provide will be dealt with in accordance with our '}
					<Link className={styles.link} to="/privacy-collection-statement/">Privacy Collection Statement</Link>
					{'. By Creating an account you agree to our '}
					<Link className={styles.link} to="/privacy-collection-statement/">Terms and Conditions</Link>
					{'.'}
				</div>
				<Button {...BUTTONS.SIGN_UP[0]} />
				<Button {...BUTTONS.SIGN_UP[1]} />
			</form>
		</Fragment>
	);

};

export default SignUp;