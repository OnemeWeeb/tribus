import React from 'react';

const ArrowIcon = ({ select }) => (
	<svg
		xmlns="http://www.w3.org/2000/svg"
		height="20px"
		width={select ? "9px" : "8"}
		fill={select ? "#fff" : "#35D0DE"}
		viewBox="0 0 10 18">
		<path d="M.347 17.646c.23.236.52.354.838.354.29 0 .607-.118.838-.354l7.63-7.8C9.884 9.62 10 9.325 10 9s-.116-.62-.347-.856l-7.63-7.8a1.17 1.17 0 0 0-1.676 0 1.23 1.23 0 0 0 0 1.711L7.14 9 .347 15.934a1.23 1.23 0 0 0 0 1.711z"></path>
	</svg>
);

export default ArrowIcon;