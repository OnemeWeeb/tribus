import ArrowIcon from './ArrowIcon';
import EventsIcon from './EventsIcon';
import GrantsIcon from './GrantsIcon';
import LocIcon from './LocIcon';
import ProjectsIcon from './ProjectsIcon';
import SearchIcon from './SearchIcon';

export {
  ArrowIcon,
  EventsIcon,
  GrantsIcon,
  LocIcon,
  ProjectsIcon,
  SearchIcon,
};
