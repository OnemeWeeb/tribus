import React from 'react';

import Routes from '../containers/Routes/RoutesContainer';

import './App.scss';

const App = () => (
  <div className='app'>
    <Routes />
  </div>
);

export default App;
