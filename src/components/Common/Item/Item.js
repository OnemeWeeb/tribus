import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import ProjectsIcon from '../../SVG/ProjectsIcon';
import LocIcon from '../../SVG/LocIcon'

import styles from './Item.module.scss';

const Item = ({ type, name, location, user, description }) => {
	const [state, setState] = useState({});

	const handleChange = ({ name, value }) => {
		setState({
			...state,
			[name]: value,
		})
	}

	return (
		<div className={styles.item}>
			<div className={styles.head}>
				<div className={styles.item_icon}>
					<ProjectsIcon />
					<span className={styles.item_name}>{`${'Community'} ${type}`}</span>
				</div>
				<Link to="/" className={styles.item_img}>
					<img className={styles.img} alt={`${type}_image`} />
				</Link>
			</div>
			<div className={styles.body}>
				<Link to="/" className={styles.item_avatar}>
					<img className={styles.avatar_img} alt="alt" />
				</Link>
				<Link to="/" className={styles.item_title}>{name}</Link>
				<div className={styles.item_loc}>
					<LocIcon />
					{location}
				</div>
				<Link to="/" className={styles.item_user}>{user}</Link>
				<div className={styles.item_description}>{description}</div>
				<button className={styles.item_button} type="submit">{'Discuss Project'}</button>
			</div>
		</div>
	);

};

export default Item;