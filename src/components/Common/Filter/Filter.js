import React from 'react';
import Select from 'react-select';

import ArrowIcon from '../../SVG/ArrowIcon';

import styles from './Filter.module.scss';

const Filter = ({ name, target, placeholder, options, value, checkbox, handleChange }) => {
	const customStyles = {
		control: (provided, state) => ({
			position: 'relative',
			borderRadius: '25px',
			border: '1px solid',
			borderColor: state.menuIsOpen ? '#35d0de' : '#fff',
			fontSize: '18px',
			height: '50px',
			paddingLeft: '20px',
			fontWeight: 600,
			lineHeight: '50px',
			color: '#333',
			zIndex: state.menuIsOpen ? 50 : 40,
			cursor: 'default',
			backgroundColor: '#fff',
		}),
		valueContainer: (provided) => ({
			...provided,
			position: 'initial',
			padding: 0,
		})
	}

	const customComponents = {
		IndicatorSeparator: () => null,
		Placeholder: ({ children }) => (
			<div children={children} className={`${styles.select_text} ${styles.select_placeholder}`} />
		),
		SingleValue: ({ children }) => (
			<div children={children} className={`${styles.select_text} ${styles.select_value}`} />
		),
		Input: () => null,
		IndicatorsContainer: ({ innerProps, children }) => (
			<div {...innerProps} children={children} className={styles.wrap_select_icon} />
		),
		DropdownIndicator: ({ innerProps, selectProps }) => (
			<div {...innerProps} className={`${styles.select_icon} ${selectProps.menuIsOpen ? styles.select_open_icon : ''}`}>
				<ArrowIcon select={true} />
			</div>
		),
		Menu: ({ innerProps, children }) => (
			<div {...innerProps} children={children} className={styles.menu_outer} />
		),
		MenuList: ({ innerProps, children }) => checkbox ? (
			<div {...innerProps} children={children} className={styles.checkbox_menu} />
		) : (
				<div {...innerProps} children={children} className={styles.menu} />
			),
		Option: ({ innerProps, data, isFocused }) => checkbox ? (
			<label {...innerProps} className={styles.checkbox_item}>
				{data.label}
				<span className={styles.checkbox} />
			</label>
		) : (
				<div {...innerProps} {...isFocused} className={`${styles.option} ${isFocused ? styles.focused : ''}`}>
					{data.label}
				</div>
			),
	}

	return (
		<Select
			options={options}
			styles={customStyles}
			components={customComponents}
			className={styles.filter}
			closeMenuOnSelect={!checkbox}
			placeholder={placeholder}
		/>
	);
}

export default Filter;