import React from 'react';
import { Link } from 'react-router-dom';

import ArrowIcon from '../../SVG/ArrowIcon';

import styles from './Pagination.module.scss';

const Pagination = ({ page, changePage }) => {

	return (
		<div className={styles.pagination}>
			<Link to="/" className={`${styles.button} ${styles.item} ${styles.prev_item} ${styles.disabled}`}>
				<ArrowIcon />
			</Link>
			<Link to="/" className={`${styles.item} ${styles.active}`}>1</Link>
			<Link to="/" className={`${styles.item}`}>2</Link>
			<Link to="/" className={`${styles.item}`}>3</Link>
			<Link to="/" className={`${styles.item}`}>4</Link>
			<Link to="/" className={`${styles.item}`}>5</Link>
			<Link to="/" className={`${styles.item}`}>6</Link>
			<Link to="/" className={`${styles.item}`}>7</Link>
			<Link to="/" className={`${styles.button} ${styles.item} ${styles.left_right}`}>
				<ArrowIcon />
			</Link>
		</div>
	);

};

export default Pagination;