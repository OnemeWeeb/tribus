import React from 'react';
import { Link } from 'react-router-dom';

import styles from './Button.module.scss';

const Button = ({ name, place, type, buttonStyle, handleClick, link }) => (
	<div className={styles[`wrap_${place}`]}>
		{link
			? <Link className={`${styles.button} ${styles.primary}`} to={link}>{name}</Link>
			: <button type={type || "button"} className={`${styles.button} ${styles[buttonStyle]}`} onClick={handleClick}>{name}</button>
		}

	</div>
);

export default Button;