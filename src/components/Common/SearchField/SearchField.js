import React from 'react';

import SearchIcon from '../../SVG/SearchIcon';

import styles from './SearchField.module.scss';

const SearchField = ({ placeholder }) => (
	<div className={styles.search_field}>
		<div className={styles.wrap_icon}>
			<SearchIcon field={true}/>
		</div>
		<div className={styles.field}>
			<label className={styles.label}>
				<input className={styles.input} placeholder={placeholder}/>
			</label>
		</div>
		<div className={styles.wrap_button}>
			<button type="submit" className={styles.button}>Find</button>
		</div>
	</div>
);

export default SearchField;