import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import Filter from '../Filter/Filter';
import SearchField from '../SearchField/SearchField';

import { GrantsIcon, ProjectsIcon, EventsIcon } from '../../SVG';

import { VALUES } from '../../../utils/constants';
import { getInitialState, getOptions } from '../../../utils/search';


import styles from './Search.module.scss';

const Search = ({ data, target, getData }) => {
	const [state, setState] = useState({});

	useEffect(() => {
		Object.keys(data).forEach((item) => getData(item === 'projects' ? 'ideas' : item));
	}, [])

	const handleChange = ({ target, name, value }) => {
		setState({
			...state,
			[target]: {
				...state[target],
				[name]: value,
			}
		})
	}

	return (
		<div className={`${styles.search_wrap} ${styles[`find_${target}`]}`}>
			{console.log(data)}
			{/* <div className={styles.search}>
				<ul className={styles.panel}>
					<li className={styles.panel_item}>
						<Link to="/" className={styles.link}>
							<div className={`${styles.item_icon} ${(target === 'projects') ? styles.active_item_icon : ''}`}>
								<ProjectsIcon isActive={target === 'projects'} />
							</div>
							<div className={`${styles.item_name} ${(target === 'projects') ?styles.active_item_name : ''}`}>Projects</div>
						</Link>
					</li>
					<li className={styles.panel_item}>
						<Link to="/" className={styles.link}>
							<div className={`${styles.item_icon} ${(target === 'grants') ? styles.active_item_icon : ''}`}>
								<GrantsIcon isActive={target === 'grants'} />
							</div>
							<div className={`${styles.item_name} ${(target === 'grants') ?styles.active_item_name : ''}`}>Grants</div>
						</Link>
					</li>
					<li className={styles.panel_item}>
						<Link to="/" className={styles.link}>
							<div className={`${styles.item_icon} ${(target === 'events') ? styles.active_item_icon : ''}`}>
								<EventsIcon isActive={target === 'events'} />
							</div>
							<div className={`${styles.item_name} ${(target === 'events') ?styles.active_item_name : ''}`}>Events</div>
						</Link>
					</li>
				</ul>
				<div className={styles.forms}>
					<div className={styles.form_name}>
						{'Find Projects '}
						<span className={styles.form_name_subtitle}>that matter to you</span>
					</div>
					<form>
						<div className={styles.wrap_search_field}>
							<SearchField placeholder="Search Projects by Address, Suburb, or State" />
						</div>
						<div className={styles.filters}>
							<Filter name={'sort_by'} target={'projects'} placeholder="Sort by..." options={getOptions(VALUES.SEARCH.PROJECTS.sort_by)} value={state.projects.sort_by} handleChange={handleChange}/>
							<Filter placeholder="Active modules" options={getOptions(VALUES.SEARCH.PROJECTS.active_modules)} value={state.projects.active_modules} handleChange={handleChange} checkbox={true}/>
							<Filter placeholder="Choose category" options={VALUES.SEARCH.choose_category} value={state.choose_category}/>
							<Filter placeholder="Choose benefits" options={VALUES.SEARCH.choose_benefits} value={state.choose_benefits}/>
						</div>
					</form>
				</div>
			</div> */}
		</div>
	);

};

export default Search;