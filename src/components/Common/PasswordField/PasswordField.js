import React from 'react';

import TextField from '../TextField/TextField';

import styles from './PasswordField.module.scss';

const PasswordField = ({ input, header, placeholder, value, handleChange, isShowing, handleClick }) => (
	<div className={styles.password_field}>
		{console.log(header)}
		<TextField
			header={header}
			input={input}
			value={value}
			type={isShowing ? 'text' : 'password'}
			placeholder={placeholder}
			handleChange={(e) => handleChange(e)}
		/>
		<div className={`${styles.visibility} ${isShowing ? styles.show : styles.hide}`} onClick={() => handleClick()} />
	</div>
);

export default PasswordField;