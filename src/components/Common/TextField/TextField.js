import React from 'react';

import styles from './TextField.module.scss';

const TextField = ({ input, type, placeholder, header, handleChange, value }) => (
	<label className={(type !== 'password') ? styles.text_field : styles.password_field}>
		{(header) && <div className={styles.header}>{`${input.name[0].toUpperCase() + input.name.substring(1)}:`}</div>}
		<input
			{...input}
			type={type}
			value={value}
			placeholder={placeholder[0].toUpperCase() + placeholder.substring(1)}
			className={styles.input}
			onChange={(e) => handleChange(e)}
		/>
	</label>
);

export default TextField;