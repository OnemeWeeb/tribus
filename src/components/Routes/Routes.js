import React from 'react';
import { createBrowserHistory } from 'history';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Header from '../../containers/PageComponents/Header/HeaderContainer';
import Footer from '../PageComponents/Footer/Footer';
import HomePage from '../../containers/HomePage/HomePageContainer';
import SignUpPage from '../../components/Pages/SignUpPage/SignUpPage';
import ModalWindow from '../../containers/PageComponents/ModalWindow/ModalWindowContainer';

import { ROUTES } from '../../utils/constants';

const Routes = ({ modalWindow }) => (
  <BrowserRouter history={createBrowserHistory()}>
    <Header />
    <Switch>
      <Route path="/" exact render={() => <HomePage />} />
			<Route path={ROUTES.SIGN_UP} exact render={() => <SignUpPage />} />
    </Switch>
    <Footer />
    {modalWindow && <ModalWindow modalWindow={modalWindow} />}
  </BrowserRouter>
);

export default Routes;