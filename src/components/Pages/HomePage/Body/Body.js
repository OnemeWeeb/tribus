import React, { useState } from 'react';

import Pagination from '../../../Common/Pagination/Paginations'

import styles from './Body.module.scss';

const Body = () => {
	const [state, setState] = useState({});

	const handleChange = ({ name, value }) => {
		setState({
			...state,
			[name]: value,
		})
	}

	return (
		<div className={styles.wrap_body}>
			<div className={styles.body}>
				<div className={styles.title}>{`Showing filtered ${'Projects'} results ${'without filters'}`}</div>
				<div className={styles.list}></div>
				<Pagination />
			</div>
		</div>
	);

};

export default Body;