import React, { useState } from 'react';

import Search from '../../../containers/Search/SearchContainer';
import Body from './Body/Body';

import { VALUES } from '../../../utils/constants';

import styles from './HomePage.module.scss';

const HomePage = () => {
	const [state, setState] = useState(VALUES.MAIN);

	const handleChange = ({ name, value }) => {
		setState({
			...state,
			[name]: value,
		})
	}

	return (
		<div className={styles.home_page}>
			<Search />
			<Body />
		</div>
	);

};

export default HomePage;