import React from 'react';

import SignUp from '../../../containers/SignUp/SignUpContainer';

import styles from './SignUpPage.module.scss';

const SignUpPage = () => (
	<div className={styles.sign_up}>
		<div className={styles.content_wrap}>
			<div className={styles.sign_up_form}>
				<SignUp />
			</div>
		</div>
	</div>
);

export default SignUpPage;