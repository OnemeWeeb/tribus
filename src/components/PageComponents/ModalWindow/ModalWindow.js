import React, { Fragment } from 'react';

import styles from './ModalWindow.module.scss';
import getComponent from '../../../utils/modalWindow';

const ModalWindow = ({ hide, modalWindow }) => (
	<Fragment>
		<div className={styles.overlay} />
		<div className={styles.wrap_window} onClick={(e) => {
			if (e.target.className === styles.wrap_window) {
				return hide();
			}
		}}>
			<div className={`${styles.modal_window} ${styles[modalWindow]}`}>
				<div className={styles.content}>
					{getComponent(modalWindow)}
				</div>
			</div>
		</div>
	</Fragment>
);

export default ModalWindow;