import React from 'react';
import { Link } from 'react-router-dom';

import styles from './Footer.module.scss';

const Footer = () => (
	<footer className={styles.footer}>
		<div className={styles.head}>
			<h2 className={styles.head_title}>Keep up to date with Tribus</h2>
			<button className={styles.head_button}>Subscribe</button>
		</div>
		<div className={styles.body}>
			<div className={styles.links}>
				<div className={styles.title}>Getting Started</div>
				<div className={styles.list}>
					<Link to="/" className={styles.item}>How Tribus Works</Link>
					<Link to="/" className={styles.item}>Tribus FAQs</Link>
					<Link to="/" className={styles.item}>Explore projects</Link>
					<Link to="/" className={styles.item}>Create a project</Link>
				</div>
			</div>
			<div className={styles.links}>
				<div className={styles.title}>About</div>
				<div className={styles.list}>
					<Link to="/" className={styles.item}>About Tribus</Link>
					<Link to="/" className={styles.item}>Contact us</Link>
					<Link to="/" className={styles.item}>Terms and Conditions</Link>
					<Link to="/" className={styles.item}>Privacy Policy</Link>
				</div>
			</div>
			<Link to="/" className={styles.logo}>
				<img className={styles.logo_img} src={`${process.env.PUBLIC_URL}/img/logo.svg`} alt="brand" />
			</Link>
		</div>
	</footer>
);

export default Footer;