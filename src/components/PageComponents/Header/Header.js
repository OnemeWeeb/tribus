import React, { Fragment, useState } from 'react';
import { Link } from 'react-router-dom';

import SearchIcon from '../../SVG/SearchIcon';
import Button from '../../Common/Button/Button';

import { BUTTONS } from '../../../utils/constants';

import styles from './Header.module.scss';

const Header = ({ isAuthorized, show }) => {
  const [state, setState] = useState({});

  const handleChange = ({ name, value }) => {
    setState({
      ...state,
      [name]: value,
    })
  }

  return (
    <div className={styles.header_wrap}>
      <header className={styles.header}>
        <div className={styles.logo_wrap}>
          <Link to="/" className={styles.logo}>
            <img className={styles.logo_img} src={`${process.env.PUBLIC_URL}/img/logo.svg`} alt="brand" />
          </Link>
        </div>
        <div className={styles.menu}>
          <div className={styles.list}>
            <Link to="/" className={styles.nav}>
              <svg height="22px" width="28px" fill="#3b393a" className={styles.search_img}>
                <SearchIcon />
              </svg>
              <span>Find</span>
            </Link>
            <Link to="/" className={styles.nav}>How Tribus works</Link>
            {isAuthorized && <Link className={styles.nav}>Feed</Link>}
          </div>
          <div className={styles.list}>
            {isAuthorized
              ? <Fragment>
                <Button {...BUTTONS.AUTH_HEADER[0]}/>
                <Button {...BUTTONS.AUTH_HEADER[1]}/>
              </Fragment>
              : <Fragment>
                <Button {...BUTTONS.NOT_AUTH_HEADER[0]} handleClick={() => show('sign_in')}/>
                <Button {...BUTTONS.NOT_AUTH_HEADER[1]} handleClick={() => show('sign_in')}/>
                <Button {...BUTTONS.NOT_AUTH_HEADER[2]} link="/sign-up/"/>
              </Fragment>}
          </div>
        </div>
      </header>
    </div>
  );

};

export default Header;