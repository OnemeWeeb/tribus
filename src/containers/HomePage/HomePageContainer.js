import { connect } from 'react-redux';

import HomePage from '../../components/Pages/HomePage/HomePage';

const mapStateToProps = ({
	dataState: { projects, grants, events }
}) => ({ projects, grants, events });

export default connect(mapStateToProps)(HomePage);