import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { reduxForm } from 'redux-form'

import SignIn from '../../components/User/SignIn/SignIn';

import { showModalWindow }  from '../../store/actions/modalWindow';
import { signInAction } from '../../store/actions/user';

const mapDispatchToProps = {
	signIn: signInAction,
	show: showModalWindow,
};

export default compose(
	reduxForm({
		formState: 'SignIn',
	}),
	connect(null, mapDispatchToProps),
	withRouter,
)(SignIn);