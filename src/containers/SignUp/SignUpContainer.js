import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { reduxForm } from 'redux-form'

import SignUp from '../../components/User/SignUp/SignUp';

import { signUpAction } from '../../store/actions/user';

const mapDispatchToProps = {
	signUp: signUpAction,
};

export default compose(
	reduxForm({
		formState: 'signUp',
	}),
	connect(null, mapDispatchToProps),
	withRouter,
)(SignUp);