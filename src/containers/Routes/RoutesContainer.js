import { connect } from 'react-redux';

import Routes from '../../components/Routes/Routes';

const mapStateToProps = ({
	modalWindowState: { modalWindow },
}) => ({ modalWindow });

export default connect(mapStateToProps)(Routes);