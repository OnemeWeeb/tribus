import { connect } from 'react-redux';

import Header from '../../../components/PageComponents/Header/Header';
import { showModalWindow } from '../../../store/actions/modalWindow';

const mapStateToProps = ({
	userState: { isAuthorized }
}) => ({ isAuthorized });

const mapDispatchToProps = {
	show: showModalWindow,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);