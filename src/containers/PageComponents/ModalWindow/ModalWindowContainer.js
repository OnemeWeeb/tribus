import { connect } from 'react-redux';

import ModalWindow from '../../../components/PageComponents/ModalWindow/ModalWindow';

import { hideModalWindow } from '../../../store/actions/modalWindow';

const mapDispatchToProps = {
	hide: hideModalWindow,
};

export default connect(null, mapDispatchToProps)(ModalWindow);