import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Search from '../../components/Common/Search/Search';

import { getDataList } from '../../store/actions/data';

const mapStateToProps = ({
	dataState: { data, loading, error, target },
}) => ({ data, loading, error, target });

const mapDispatchToProps = {
	getData: getDataList,
};

export default compose(
	connect(mapStateToProps, mapDispatchToProps),
	withRouter,
)(Search);