import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'

import modalWindowReducer from './modalWindow/modalWindow';
import userReducer from './user/user';
import dataReducer from './data/data';

const globalReducer = combineReducers({
	modalWindowState: modalWindowReducer,
	userState: userReducer,
	dataState: dataReducer,
	formState: formReducer,
});

export default globalReducer;