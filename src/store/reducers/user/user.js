import { ACTIONS } from '../../utils/constants';

const initialState = () => ({
	userInfo: {},
	isAuthorized: false,
});

const userReducer = (state = initialState(), { type, payload }) => {
	switch (type) {
	case ACTIONS.FILL_USER_INFO:
		return {
			...state,
			userInfo: payload,
			isAuthorized: true,
		};
	case ACTIONS.CLEAR_USER_INFO:
		return initialState();
	default:
		return state;
	}
};

export default userReducer;