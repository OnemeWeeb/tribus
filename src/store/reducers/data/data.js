import { ACTIONS } from '../../utils/constants';

const getInitialState = () => ({
	data: {
		projects: {
			sort_by: [
				'Popularity',
				'Max supporters',
				'Min supporters',
				'Oldest',
				'Newest',
			],
			active_modules: [
				'Volunteering',
				'Petition',
				'Crowdfunding',
			],
		},
		grants: {},
		events: {},
		benefits: {},
		categories: {},
	},
	target: 'projects',
	loading: false,
	error: false,
});

const dataReducer = (state = getInitialState(), { type, payload }) => {
	switch (type) {
		case ACTIONS.GET_DATA_LIST:
			return {
				...state,
				loading: true,
				error: false,
			};
		case ACTIONS.FILL_DATA_LIST:
			return {
				...state,
				loading: false,
				error: false,
				data: {
					...state.data,
					[payload.target]: payload.data,
				},
			};
		case ACTIONS.REQUESTED_DATA_FAILED:
			return {
				...state,
				loading: false,
				error: true,
			};
		default:
			return state;
	}
};

export default dataReducer;