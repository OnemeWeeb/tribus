import { ACTIONS } from '../../utils/constants';

const initialState = () => ({
	modalWindow: false,
});

const appReducer = (state = initialState(), { type, payload }) => {
	switch (type) {
	case ACTIONS.SHOW_MODAL_WINDOW:
		return {
			...state,
			modalWindow: payload,
		};
	case ACTIONS.HIDE_MODAL_WINDOW:
		return initialState();
	default:
		return state;
	}
};

export default appReducer;