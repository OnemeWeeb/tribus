import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import globalReducer from './reducers/globalReducer';
import rootSaga from './sagas/rootSaga';

const saga = createSagaMiddleware();
const devTools = window.devToolsExtension ? window.devToolsExtension() : f => f;
const store = createStore(globalReducer, compose(applyMiddleware(saga), devTools));

saga.run(rootSaga);
export default store;