import api from '../utils/api';

import { URL } from '../utils/constants'

export const getData = async (target) => api.axios.get(URL.GET_DATA_LIST(target));
