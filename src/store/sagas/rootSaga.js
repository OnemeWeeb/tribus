import { all } from 'redux-saga/effects';

import getDataSaga from '../sagas/data';


export default function* rootSaga() {
	yield all([
		getDataSaga(),
	])
}
