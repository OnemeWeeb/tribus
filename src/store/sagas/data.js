/* eslint-disable default-case */
import { all, call, put, takeEvery } from 'redux-saga/effects';
import { getData } from '../requests/data';
import { fillDataList, getRequestError } from '../actions/data';
import { ACTIONS } from '../utils/constants';

function* getDataWork({ payload }) {
	try {
		const response = yield call(getData, payload);
		const data = {
			data: response,
			target: payload,
		}
		yield put(fillDataList(data));
	} catch (e) {
		yield put(getRequestError);
	}
}

export function* getDataWatch() {
	yield takeEvery(ACTIONS.GET_DATA_LIST, getDataWork)
}

export default function* getDataSaga() {
	yield all([
		getDataWatch(),
	])
}