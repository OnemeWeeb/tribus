import { ACTIONS } from '../utils/constants';

export const fillDataList = (payload) => ({
	type: ACTIONS.FILL_DATA_LIST,
	payload,
});

export const getRequestError = () => ({
	type: ACTIONS.REQUESTED_DATA_FAILED,
})

export const getDataList = (payload) => ({
	type: ACTIONS.GET_DATA_LIST,
	payload,
})
