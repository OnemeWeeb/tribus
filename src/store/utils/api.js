import axios from 'axios';

class Api {
	constructor() {
		this.token = localStorage.getItem('token');
	}

	initApi() {
		axios.interceptors.request.use((config) => {
			config.headers = {
				...config.headers,
				Authorization: this.token,
				'Content-Type': 'application/json',
			};
			return config;
		});
		return axios;
	}

	get axios() {
		return this.initApi();
	}
}

const api = new Api();

export default api;