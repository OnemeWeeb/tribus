## Simple chat

#### Used technologies
- [Axios](https://github.com/axios/axios)
- [Create React App](https://facebook.github.io/create-react-app/)
- [Formik](https://formik.org/)
- [React](https://reactjs.org/)
- [Redux](https://redux.js.org/)

#### Coding style
- [Eslint](https://eslint.org/)

#### Development
Preparation:
1. `npm install` - to install packages;

Start development server:
1. `npm start` - to start development server.

Development domain:
- `http://localhost:3000`